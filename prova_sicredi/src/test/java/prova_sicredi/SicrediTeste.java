package prova_sicredi;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;

public class SicrediTeste {
	
	WebDriver driver;


	@Before
	public void setUp() throws Exception {
		
		System.setProperty("webdriver.chrome.driver", "./Drivers/chromedriver.exe");
		driver = new ChromeDriver();
		driver.manage().window().maximize();
	}

	@After
	public void tearDown() throws Exception {
		
		driver.quit();
	}

	@Test
	public void test() {
		driver.get("https://www.grocerycrud.com/v1.x/demo/my_boss_is_in_a_hurry/bootstrap");

		driver.findElement(By.xpath("//*[@id=\"switch-version-select\"]")).sendKeys("Bootstrap V4 Theme");
		driver.findElement(By.cssSelector("#gcrud-search-form > div.header-tools > div.floatL.t5 > a")).click();
		driver.findElement(By.xpath("//*[@id=\"field-customerName\"]")).sendKeys("Teste Sicredi");
		driver.findElement(By.xpath("//*[@id=\"field-contactLastName\"]")).sendKeys("Teste");
		driver.findElement(By.xpath("//*[@id=\"field-contactFirstName\"]")).sendKeys("Tayna");
		driver.findElement(By.xpath("//*[@id=\"field-phone\"]")).sendKeys("51 9999-9999");
		driver.findElement(By.xpath("//*[@id=\"field-addressLine1\"]")).sendKeys("Av Assis Brasil, 3970");
		driver.findElement(By.xpath("//*[@id=\"field-addressLine2\"]")).sendKeys("Torre D");
		driver.findElement(By.xpath("//*[@id=\"field-city\"]")).sendKeys("Porto Alegre");
		driver.findElement(By.xpath("//*[@id=\"field-state\"]")).sendKeys("RS");
		driver.findElement(By.xpath("//*[@id=\"field-postalCode\"]")).sendKeys("91000-000");
		driver.findElement(By.xpath("//*[@id=\"field-country\"]")).sendKeys("Brasil");
		driver.findElement(By.xpath("//*[@id=\"field-salesRepEmployeeNumber\"]")).sendKeys("Fixter");
		driver.findElement(By.xpath("//*[@id=\"field-creditLimit\"]")).sendKeys("200");
		driver.findElement(By.xpath("//*[@id=\"form-button-save\"]/font/font")).click();

	}

}
